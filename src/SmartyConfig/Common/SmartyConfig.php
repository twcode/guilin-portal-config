<?php
namespace SmartyConfig\Common;

class SmartyConfig
{
    public static function configDir()
    {
        $dirArray = array(
            S_ROOT.'vendor/twcode/guilin-portal-config/src/SmartyConfig/Gl',
            S_ROOT.'vendor/twcode/guilin-portal-config/src/SmartyConfig/Common',
            S_ROOT.'vendor/twcode/guilin-portal-config/src/SmartyConfig/',
        );

        return $dirArray;
    }
}
